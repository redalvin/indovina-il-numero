package Client;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Scanner;

public class Client {

    static Socket socketServer = null;
    public static final int PORT = 9999;

    static DataInputStream in;
    static DataOutputStream out;
    static Scanner sc = new Scanner(System.in);

    //Controlla se la stringa inserita dall'utente è giusto
    private static boolean checkInput(String s) {
        ArrayList<Character> arrayList = new ArrayList<>();
        for (char c : s.toCharArray()) {
            if (arrayList.contains(c))
                return false;
            else
                arrayList.add(c);
        }
        for (char c : s.toCharArray()) {
            if (!Character.isDigit(c))
                return false;
        }
        return true;
    }

    //Metodo utilizzato per connettere il client al server
    public Socket connection() {
        Socket server = null;
        try {
            System.out.println("Tento la connessione con il server di gioco......");
            server = new Socket("localhost", PORT);
            System.out.println("Connessione con il server stabilita\n");
            in = new DataInputStream(server.getInputStream());
            out = new DataOutputStream(server.getOutputStream());
        } catch (UnknownHostException e) {
            System.err.println("Hpst sconosciuto");
        } catch (Exception e) {
            System.err.println("Impossibile connettersi all'host");
            System.exit(1);
        }
        return server;
    }

    //Metodo utilizzato per leggere la risposta del server
    public String readAnswer(){
        String s = "";
        try {
            s = in.readUTF();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(s.equals("Hai vinto, hai indovinato il numero!")) {
            System.out.println(s);
            System.exit(0);
        }
        return s;
    }

    public static void main(String[] args) {
        String s = "";
        System.out.println("\t\tIndovina il numero! V1.0\n");
        System.out.println("Inziamo il gioco!");
        Client c = new Client();
        socketServer = c.connection();
        try {
            in = new DataInputStream(new BufferedInputStream(socketServer.getInputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        while(true) {
            System.out.print("Prova ad indovinare il numero: ");
            s = sc.nextLine();
            if (checkInput(s)) {
                try {
                    out.writeUTF(s);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                System.out.println(c.readAnswer());
            } else {
                System.out.println("Hai inserito un input sbagliato!");
            }
        }
    }
}
