package Server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    public static final int PORT = 9999;
    static Socket socket = null;
    static ServerSocket serverSocket = null;

    public static void main(String[] args) {
        try{
            serverSocket = new ServerSocket(PORT);
        } catch (IOException e) {
            e.printStackTrace();
        }
        while(true){
            try{
                socket = serverSocket.accept();
            } catch (IOException e) {
                e.printStackTrace();
            }
            ServerThread serverThread = new ServerThread(socket); //La creazione di più thread permette il multi utente
            serverThread.start(); //Fa partire il gioco sul server in un thread dedicato
        }
    }
}
