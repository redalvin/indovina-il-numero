package Server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Random;

public class ServerThread extends Thread {

    private ArrayList<Integer> mysteriousNumberList = new ArrayList<>();
    Socket socketClient = null;
    String mysteriousNumberString = "";

    DataInputStream in;
    DataOutputStream out;

    //Il metodo controlla quanti numeri giusti ci sono nell'input
    private int checkRightNumber(ArrayList<Integer> userNumber, ArrayList<Integer> mysteriousNumber){
        int n = 0;
        for(Integer i : userNumber){
            if(mysteriousNumber.contains(i))
                n++;
        }
        return n;
    }

    //Il metodo controlla quanti numeri in posizione giusta ci sono
    private int checkRightPosition(ArrayList<Integer> userNumber, ArrayList<Integer> mysteriousNumber){
        int n = 0;
        for(int i = 0; i < 4; i++){
            if(mysteriousNumber.get(i).equals(userNumber.get(i)))
                n++;
        }
        return n;
    }

    //Il metodo genera il numero casuale (controllando che non ci siano numeri uguali)
    private ArrayList<Integer> generateMysteriousNumber(){
        int n = 0;
        while(mysteriousNumberList.size() < 4){
            Random rnd = new Random();
            n = rnd.nextInt(10); //genera il numero casuale
            if(!mysteriousNumberList.contains(n))
                mysteriousNumberList.add(n);
        }
        return mysteriousNumberList;
    }

    //Converte il contenuto della stringa dentro ad un array list di interi
    private ArrayList<Integer> inputToInt(String s){
        ArrayList<Integer> list = new ArrayList<>();
        for(int i = 0; i < s.length(); i++)
            list.add(Character.getNumericValue(s.charAt(i)));  //Questo metodo permette di ottenere il numero contenuto della string
        return list;
    }

    //Converte il contenuto dell'array list in una stringa
    private String inputToString(ArrayList<Integer> list){
        StringBuilder s = new StringBuilder();
        for(Integer i : list){
            s.append(i.toString());
        }
        return s.toString();
    }

    ServerThread(Socket socket){
        socketClient = socket;
        mysteriousNumberList = generateMysteriousNumber();
        mysteriousNumberString = inputToString(mysteriousNumberList);
    }

    //Il metodo legge la richiesta del server
    public String readClient(){
        try{
            in = new DataInputStream(socketClient.getInputStream());
        } catch(Exception e){
            e.printStackTrace();
        }
        String s = "";
        try {
            s = in.readUTF();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return s;
    }

    @Override
    public void run(){
        try{
            out = new DataOutputStream(socketClient.getOutputStream());
        }catch(Exception e){
            e.printStackTrace();
        }
        System.out.println(mysteriousNumberString);
        while(true){
            String clientRead = readClient();
            int rightPositions = checkRightPosition(mysteriousNumberList, inputToInt(clientRead));
            int rightNumbers = checkRightNumber(mysteriousNumberList, inputToInt(clientRead));
            if(clientRead.equals(mysteriousNumberString)){
                try {
                    out.writeUTF("Hai vinto, hai indovinato il numero!");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            } else {
                try {
                    out.writeUTF("Numeri presenti giusti presente (ma non nella posizione giusta): " + (rightNumbers - rightPositions) + "\n" + "Numeri nella giusta posizione: " + rightPositions + "\n");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }


}